# GitLab Runner

This assumes you've configured the daemon as a Docker Swarm manager. If you haven't done so, just run `docker swarm init`.

## Bootstrap GitLab runner on host

Run the following to initially setup the gitlab runner.

```shell
git clone https://gitlab.com/notno/gitlab-runner.git
cd gitlab-runner
docker stack deploy -c docker-compose.yml --prune gitlab-runner
```

You can now remove the just cloned repo.

## Add runner

Now we need to register a project or group with the runner, by running this on the host where you just added the runner. This can be run either interactively:

```shell
source <(curl -sSL "https://gitlab.com/notno/gitlab-runner/-/raw/master/register.sh")
```

Or scripted, ensuring you replace the variables below with the required values.

```shell
curl -sSL "https://gitlab.com/notno/gitlab-runner/-/raw/master/register.sh" | bash -s -- $GITLAB_URL $RUNNER_TOKEN $DESCRIPTION
```

### Privileged

Add `/var/run/docker.sock:/var/run/docker.sock` to the volumes key in `config.toml` to give a runner access to the deamon. This can be useful when you want to deploy containers on that host using the runner.

Of course make sure only trusted parties get access to this runner.

## Keeping runner up to date

Create a project which has access to the runner, which also is priviliged as per the previous paragraph. Then add this to the project's `.gitlab-ci.yml`:

```yaml
---
include:
  remote: "https://gitlab.com/notno/gitlab-runner/-/raw/master/update.yml"
```

I use tags to ensure this job gets run on a privileged runner, but that other jobs with different or without tags never run on that runner.

For that, I add the following to `.gitlab-ci.yml`:
```yaml
update:
  tags:
    - privileged
``` 

Make sure to schedule the job in GitLab.
