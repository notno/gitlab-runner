#!/usr/bin/env bash

RUNNER_CONTAINER=gitlab-runner_gitlab-runner.1.$(docker service ps -f name="gitlab-runner_gitlab-runner".1 "gitlab-runner_gitlab-runner" -q --no-trunc | head -n1)

if [ -z "${RUNNER_CONTAINER}" ]; then
    echo "[!] GitLab Runner container not found. See README: https://gitlab.com/notno/gitlab-runner."
    exit 1
fi

if [ -z "$3" ]; then
    echo "What is the GitLab URL?"
    read GITLAB_URL
    echo "What is the runner token?"
    read RUNNER_TOKEN
    echo "What description would you give this runner?"
    read DESCRIPTION
else
    GITLAB_URL="$1"
    RUNNER_TOKEN="$2"
    DESCRIPTION="$3"
fi

docker exec "${RUNNER_CONTAINER}" gitlab-runner register -n \
    --url "${GITLAB_URL}" \
    --registration-token "${RUNNER_TOKEN}" \
    --executor docker \
    --description "${DESCRIPTION}" \
    --docker-image "docker"
